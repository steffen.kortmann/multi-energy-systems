{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "vscode": {
     "languageId": "raw"
    }
   },
   "source": [
    "# **[Bonus Task 1]** Value Chain\n",
    "Welcome to the first bonus task! This bonus task will help you to unterstand the basics of data science for energy system analysis. Make sure to run all cells before downloading and submitting the notebook to Moodle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "## ENTSO-E\n",
    "The **European Network of Transmission System Operators** represents 40 electricity transmission system operators (TSOs) from 36 countries across Europe. It was established by the EU's Third Package for the internal energy markets in 2009 with the aim to liberlise the electricity market in the EU. In here, TSOs constantly discuss topics related the further development of the electricity system in Europe. But not just this, they also define standards or share data they collect. For the safe operation of the electricity system, it is important to have an accurate forecast of the next day, 24 hours ahead. We would like to analyse how precise the forecast error is, by defining and calculating the *mean absolute error*.\n",
    "\n",
    "![load forecast error](./figures/load_forecast_error.png \"Load Forecast Error in Germany\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "### Task 1: Get a metrics from data.\n",
    "**Task Description:** With the mean absolute error as defined below, we would like to calculate this for the file from ``file_path`` which can be read in by [Pandas](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html). Read it in as a dataframe and store it in a variable called ``total_load``. Add a column ``abs_error`` in where you calculate the absolute error between the two columns from the dataframe. Then calculate the mean of the absolute error and save it in a variable called ``mae``.\n",
    "\n",
    "$$ MAE = \\frac{1}{n} \\sum_{i=1}^{n} | y_i - \\hat{y_i} | $$\n",
    "\n",
    "where,\n",
    "\n",
    "$n$: number of observation\n",
    "$y_i$: the actual value of the $i^{th}$ observation\n",
    "$\\hat{y_i}$: the forecasted value of the $i^{th}$ observation\n",
    "\n",
    "***Points***: 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "# Access to the \"data\" folder in the current directory\n",
    "parent_folder = os.path.join(os.getcwd(), \"data\")\n",
    "\n",
    "# Accessing the file in the \"data\" folder\n",
    "file_path = os.path.join(parent_folder, \"total_load_2024.csv\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "total_load = pd.read_csv(file_path)\n",
    "total_load[\"abs_error\"] = np.abs(total_load[\"Actual Total Load [MW] - BZN|DE-LU\"] - total_load[\"Day-ahead Total Load Forecast [MW] - BZN|DE-LU\"])\n",
    "mae = total_load[\"abs_error\"].mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "### Task 2: Calculation of Net Present values\n",
    "**Task Description:** The net present value is defined as \n",
    "\n",
    "$$ NPV = \\sum_{t=0}^{T} \\frac{\\text{cash flow}_t}{(1+i)^t} = I_0 + \\sum_{t=1}^{T} \\frac{\\text{cash flow}_t}{(1+i)^t}$$\n",
    "\n",
    "Calculate the net present value for unequal payments, for which we generate a list of 35 random numbers between -10 and 100 and compute the net_present_value. We use the ``random`` package for this with a seed for reproducibility. Create a function called ``net_present_value`` which takes as input parameters a list called ``cash_flow``, the lifetime ``T`` as an integer and ``interest_rate`` as a float. Return the net present value of this list.\n",
    "\n",
    "***Points***: 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "import random\n",
    "random.seed(30)\n",
    "\n",
    "T = 35\n",
    "i = 0.05\n",
    "cash_flow = random.sample(range(-10, 100), T) #Sample from a list with number between -10 and 100, 35 random numbers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def net_present_value(cash_flow, T: int, interest_rate: float):\n",
    "    \n",
    "    if isinstance(cash_flow, list):\n",
    "        return sum(cash_flow[t]/(1+interest_rate)**t for t in range(T))\n",
    "    else:\n",
    "        return sum(cash_flow/(1+interest_rate)**t for t in range(T))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "vscode": {
     "languageId": "raw"
    }
   },
   "source": [
    "### Task 3: Calculation of Payback Period\n",
    "**Task Description:** The payback period is a fundamental financial metric that measures the time required for the return on an investment to \"pay back\" the sum of the original investment. In this task, write a function that computes the payback period for a series of cash flows. Assume that the first cash flow represents the initial investment (negative value) and subsequent cash flows represent income (positive values).\n",
    "\n",
    "$$ \\text{Payback Period (CF)} = \\min \\left\\{ t \\mid \\sum_{\\tau=0}^{t} CF_{\\tau} \\geq -C_{0} \\right\\}$$\n",
    "\n",
    "where \n",
    "$C_{\\tau}$ represents the cash flow at time $\\tau$, where $C_{0}$ is the initial investment (and is negative).\n",
    "$t$ is the time period (e.g., year, month, quarter) at which the cumulative cash flows equal or exceed the initial investment.\n",
    "\n",
    "- The function starts by summing cash flows until the cumulative sum equals or exceeds the absolute value of the initial investment.\n",
    "- If cash flows do not cover the initial investment by the end of the list, the function returns None to indicate that the payback period is undefined with the given data.\n",
    "- The function can handle fractional periods by interpolating the necessary fraction of a period to achieve payback in the final year.\n",
    "\n",
    "$$ \\text{Fractional Period (CF)} = \\frac{-\\left(\\sum_{\\tau=0}^{t-1} CF_{\\tau} + CF_0\\right)}{CF_t} $$\n",
    "\n",
    "- The function should then return the correct payback period as the sum of the number of years it takes to pay back the initial investment with the fractional period of the last year.\n",
    "- The fractional term is used only when the cumulative cash flow up to period $t-1$ does not completely cover the initial investment. This fraction represents how much of period $t$ is needed in addition to the previous periods to balance the initial investment.\n",
    "\n",
    "\n",
    "***Points***: 4\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "import random\n",
    "random.seed(30)\n",
    "\n",
    "T = 35\n",
    "cash_flow = [-100] + random.sample(range(-10, 100), T) #Sample from a list with number between -10 and 100, 35 random numbers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def payback_period(cash_flows: list) -> float:\n",
    "    \"\"\"\n",
    "    Calculate the payback period for a series of cash flows.\n",
    "\n",
    "    Parameters:\n",
    "        cash_flows (list): List of cash flows, starting with the initial investment (negative),\n",
    "                           followed by periods of returns (positive).\n",
    "    \n",
    "    Returns:\n",
    "        float: The payback period in terms of the number of periods.\n",
    "               Returns None if the investment is never paid back.\n",
    "    \"\"\"\n",
    "    initial_investment = cash_flows[0]\n",
    "    cumulative_cash_flow = 0\n",
    "    \n",
    "    for period, cash_flow in enumerate(cash_flows):\n",
    "        cumulative_cash_flow += cash_flow\n",
    "        \n",
    "        if cumulative_cash_flow >= -initial_investment:\n",
    "            # Calculate fractional part if needed (for the year in which payback occurs)\n",
    "            previous_cash_flow = cumulative_cash_flow - cash_flow\n",
    "            fractional_period = (initial_investment + previous_cash_flow) / cash_flow\n",
    "            return period + fractional_period\n",
    "    \n",
    "    # If the loop completes without breaking, investment isn't paid back within the given periods\n",
    "    return None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "### Task 4: Calculation of OPEX\n",
    "**Task Description:** Develop a Python class that models transmission lines. The class should be able to store different specifications of a transmission line, such as diameter, length, material type, and voltage level. Additionally, include a method within this class that calculates the capital expenditure (CAPEX) based on these specifications and given unit costs. \n",
    "\n",
    "Class Attributes:\n",
    "- ``diameter`` (in mm)\n",
    "- ``length`` (in km)\n",
    "- ``material`` (as a string, e.g., \"Aluminum\", \"Copper\")\n",
    "- ``voltage`` level (in kV)\n",
    "\n",
    "Method:\n",
    "- ``calculate_capex``: This method will take unit cost parameters (e.g., cost per km, cost per mm diameter per km, and a base cost) and use them along with the line's attributes to compute the CAPEX.\n",
    "\n",
    "The capital expenditure for a transmission line, given its length, diameter, and specific unit costs, can be calculated using the following formula:\n",
    "\n",
    "$$ \\text{CAPEX} = C_{\\text{base}} + (L \\times C_{\\text{per km}}) + (L \\times D \\times C_{\\text{per mm per km}}) $$\n",
    "\n",
    "Where:\n",
    "- $ C_{\\text{base}} $ is the base cost, independent of line specifications.\n",
    "- $ L $ is the length of the transmission line in kilometers.\n",
    "- $ D $ is the diameter of the transmission line in millimeters.\n",
    "- $ C_{\\text{per km}} $ is the cost per kilometer of the transmission line.\n",
    "- $ C_{\\text{per mm per km}} $ is the additional cost per millimeter diameter per kilometer.\n",
    "\n",
    "This formula integrates the physical dimensions of the transmission line with the unit costs to calculate the total capital investment required.\n",
    "\n",
    "\n",
    "***Points***: 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "___\n",
    "#### Test specification\n",
    "\n",
    "<center>\n",
    "\n",
    "Specification | Index | Unit | Value\n",
    "--- | --- | --- | ---\n",
    "Diameter | $D$ | [mm] | 50\n",
    "Length | $L$ | [km] | 100\n",
    "Material | $M$ | [-] | Cooper\n",
    "Voltage | $V$ | [V] | 110\n",
    "Base Cost | $C_{base}$ | [€] | 100000\n",
    "Cost per Length | $ C_{\\text{per km}} $ | [€/km] | 5000\n",
    "Cost per Diameter | $ C_{\\text{per mm per km}} $ | [€/mm/km] | 20\n",
    "\n",
    "\n",
    "</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "class TransmissionLine:\n",
    "    def __init__(self, diameter: float, length: float, material: str, voltage: float):\n",
    "        \"\"\"\n",
    "        Initializes a new Transmission Line object with the specified attributes.\n",
    "        \n",
    "        Parameters:\n",
    "            diameter (float): The diameter of the transmission line in millimeters.\n",
    "            length (float): The length of the transmission line in kilometers.\n",
    "            material (str): The material of the transmission line (e.g., \"Aluminum\", \"Copper\").\n",
    "            voltage (float): The voltage level of the transmission line in kilovolts.\n",
    "        \"\"\"\n",
    "        self.diameter: float = diameter\n",
    "        self.length: float = length\n",
    "        self.material: str = material\n",
    "        self.voltage: float = voltage\n",
    "\n",
    "    def calculate_capex(self, base_cost, cost_per_km, cost_per_mm_per_km):\n",
    "        \"\"\"\n",
    "        Calculate the capital expenditure (CAPEX) for the transmission line.\n",
    "        \n",
    "        Parameters:\n",
    "            base_cost (float): Base cost that is independent of line specifications.\n",
    "            cost_per_km (float): Cost per kilometer of the transmission line.\n",
    "            cost_per_mm_per_km (float): Additional cost per millimeter of diameter per kilometer.\n",
    "        \n",
    "        Returns:\n",
    "            float: The total CAPEX for the transmission line.\n",
    "        \"\"\"\n",
    "        total_cost = base_cost + (self.length * cost_per_km) + (self.length * self.diameter * cost_per_mm_per_km)\n",
    "        return total_cost"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "mes_teaching_env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
