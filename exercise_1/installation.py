import shutil
import sys
import os.path
import subprocess

HIDE = True

def install(package, manager):
    if manager == "pip":
        subprocess.check_call([sys.executable, "-m", manager, "install", package], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    elif manager == "conda":
        subprocess.check_call([sys.executable, "-m", manager, "install", "-c conda-forge", package], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

# check if pyomo has been installed. If not, install with pip
pip_packages = ["pyomo"]
for package in pip_packages:
    if not shutil.which(package):
        install(package, "pip")
    assert(shutil.which(package))