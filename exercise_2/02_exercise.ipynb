{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# **[exercise 2]** Energy System Analysis and Evaluation\n",
    "In this notebook you are prompted to **understand the basics of data science** to examine *time series data* and aggregate it. Furthermore you are required to learn **basic financial mathematics** to evaluate the *profitability of financial assets*.\n",
    "\n",
    "Furthermore we will cover the following Python basics:\n",
    "- [x] Matplotlib\n",
    "- [x] Pandas\n",
    "- [x] Dictionaries\n",
    "- [x] Classes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let us import the most important packages first\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from visualization import carpet_plot\n",
    "import warnings\n",
    "\n",
    "# Suppress warnings that packages might be outdated in the future\n",
    "warnings.simplefilter(action='ignore', category=FutureWarning)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%run installation.py ;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Demand Structure\n",
    "\n",
    "A distinctive energy system definition is core to every energy system analysis and evaluation approach. In the present case the term building energy system refers to the components necessary for the energy supply of the single family home. Within this system, we focus on the components for the heat supply of the building. Those elements are schematically depicted in the following Figure 1 for the different heat supply options outlined.\n",
    "\n",
    "![Building Energy System](../../datasets/mes/exercise_2/figures/building_energy_system.png \"Building Energy System\")\n",
    "\n",
    "Figure 1: Heat supply options of a single family home with a) gas condensing boiler b) pellet heater c) air-water heat pump d) brine-water heat pump within the building energy system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "In order to find proper supply options for a building energy system, it is necessary to identify the demand structure. The essential energy demand in our scope of consideration comprises the heat demand (room heating and warm water)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import demandlib.bdew as bdew\n",
    "\n",
    "# read standard load profiles\n",
    "e_slp = bdew.ElecSlp(2021)\n",
    "\n",
    "# multiply given annual demand with timeseries\n",
    "elec_demand = e_slp.get_profile(ann_el_demand_per_sector= {\"h0_dyn\": 3000})\n",
    "elec_demand"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Be aware that the values in the DataFrame are 15 minute values with a power unit. If you sum up a table with 15min values the result will be of the unit 'kW/15minutes'. You will have to divide the result by 4 to get kWh."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "elec_demand.sum() / 4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or resample the DataFrame to hourly values using the ``.mean()`` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Resample 15-minute values to hourly values.\n",
    "elec_demand_resampled = elec_demand.resample(\"H\").mean()\n",
    "elec_demand_resampled.plot(xlabel=\"Date\", ylabel=\"Power demand\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "carpet_plot(elec_demand_resampled[\"h0_dyn\"], axis=\"electricity demand [$kWh$]\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from utils import getTemperatures\n",
    "\n",
    "# Define some Locations to choose from in Latitude and Longitude\n",
    "locations = {\n",
    "    'Aachen' : ['50.778620', '6.075170']\n",
    "}\n",
    "\n",
    "hourly_temperatures_ac = getTemperatures(locations, 'Aachen', start_date = \"2021-01-01\", end_date = \"2021-12-31\")\n",
    "df_temperature = pd.DataFrame(data = hourly_temperatures_ac[1], index = pd.to_datetime(hourly_temperatures_ac[0]), columns=[\"Aachen [°C]\"])\n",
    "df_temperature.plot(title=\"Temperature\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Single family house (efh: Einfamilienhaus)\n",
    "heat_demand = bdew.HeatBuilding(\n",
    "    pd.date_range(start='2021-01-01 00:00', end='2021-12-31 23:00', freq='H'),\n",
    "    temperature=df_temperature[\"Aachen [°C]\"],\n",
    "    shlp_type=\"EFH\",\n",
    "    building_class=1,\n",
    "    wind_class=1,\n",
    "    annual_heat_demand=25000,\n",
    "    name=\"EFH\",\n",
    ").get_bdew_profile()\n",
    "heat_demand"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot demand of building\n",
    "heat_demand.plot(xlabel=\"Date\", ylabel=\"Heat demand in [$kW$]\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "carpet_plot(heat_demand, axis=\"heat demand [$kWh_{th}$]\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 1 : Time Series Analysis <a class=\"anchor\" id=\"task-1--tsa\"></a>\n",
    "The analysis of time series data for demand is of high importance to plan the installation capacity for example heating infrastructure."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.1 : Identify the maximum heat demand <a class=\"anchor\" id=\"#task-11--identify-the-maximum-heat-demand\"></a>\n",
    "\n",
    "**Task description:** Identify the maximum and total amount of heating that is used by the househould and save it in two variables called `max_heat_demand` and `total_heat_demand`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1.3 : Calculate the heating degree days <a class=\"anchor\" id=\"#task-13--calculate-hdd\"></a>\n",
    "\n",
    "The Heating Degree Days (HDD) is a measure of how much heating is needed to maintain a comfortable temperature inside a building. If the average outdoor temperature of a day is below a certain threshold (Europe: 15.5 °C) - this is the heating limit temperature - it is called a heating day. The heating degree days are calculated by adding up the differences between the heating limit temperature and the average outdoor temperature for all heating days.\n",
    "\n",
    "$$ HDD = \\sum_{i=1}^{n} \\max(0, T_{ref} - T_{avg,i}) $$\n",
    "\n",
    "where $HDD$ is the total Heating Degree Days, $n$ is the total number of days in the period considered, $T_{ref}$ is the reference temperature and $T_{avg}$ is the average temperature. The max functions ensures to consider only the days that are below the reference temperature.\n",
    "\n",
    "**Task description:** Define a function to calculate the heating degree days."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task description:** Calculate the average temperature for each day. Store the average temperature to a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task description:** Calculate the heating degree days for a base temperature of 15.5 °C."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 1.4 - Calculate climate correction factor\n",
    "\n",
    "As found above, room-heating consumption is especially affected by different climatic conditions from year to year. In order to compare the consumption of different years, it must be weather-adjusted according to the norm VDI 2067. This resembles a normalization to a standard weather year, also called test reference year (TRY). TRY data is available for every region in Germany provided by the German Weather Service (DWD). The consumption is adjusted with the climate correction factor (CRF):\n",
    "\n",
    "$$ r_0 = \\frac{hdd_{\\text{TRY}}}{hdd_{\\text{Year}}} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the file path\n",
    "import pathlib\n",
    "\n",
    "# Access to the \"data\" folder in the current directory\n",
    "parent_folder = pathlib.Path.cwd().parents[1] / \"datasets\" / \"mes\" / \"exercise_2\" / \"data\"\n",
    "\n",
    "# Accessing the file in the \"data\" folder\n",
    "file_path = parent_folder / 'TRY_507755060854/TRY2015_507755060854_Jahr.dat'\n",
    "\n",
    "# Define the column names based on the structure\n",
    "columns = ['RW', 'HW', 'MM', 'DD', 'HH', 't', 'p', 'WR', 'WG', 'N', 'x', 'RF', 'B', 'D', 'A', 'E', 'IL']\n",
    "\n",
    "# Read the .dat file into a DataFrame\n",
    "data = pd.read_csv(file_path, skiprows=34, sep='\\s+', names=columns)\n",
    "data.index = pd.date_range(start='2015-01-01 00:00', end='2015-12-31 23:00', freq='H')\n",
    "data = data.resample(\"D\").mean()\n",
    "data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task description:** Calculate the heating degree days for the TRY weather year for Aachen, where the temperature is given by column ``data[\"t\"]``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task description:** Calculate the crf as the fraction between the heating degree days for the test reference year and the heating degree days for 2021 in Aachen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task description:** Calculate the adjusted heat demand for Aachen.\n",
    "\n",
    "$$ q_{total, adjusted} = q_{adjusted} \\times r_{Aachen} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "____\n",
    "# Economic evaluation\n",
    "Essential condition for choosing a suitable heating technology for the Potter family is a financially viable solution. As an alternative to the net present value method (see exercise 1) a commonly used method for energy system evaluation is the annuity method, assuming cash flows are equal for $t=1, \\dots, T$. Therefore the Annuity can be calculated with:\n",
    "\n",
    "$$ ANN = I_0 * AF + \\text{cash flow}_t = \\frac{I_0}{PVF} + \\text{cash flow}_t $$\n",
    "\n",
    "where $i$ is the interest rate, $T$ is the lifetime of the asset and the annuity factor $ AF = \\frac{(1+i)^T-1}{(1+i)^T \\cdot i} $."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 2.1 - Import functions from scripts\n",
    "**Task description:** Write the function to calculate the annuity factor or import it from the utils.py file to allow later usage accross other Jupyter notebooks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the following, our family Potter considers to invest in a new heating technology, as for this they have identified new options for their heating supply. Furthermore, they have collected data on the specifications for each heating technology."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Specifications for the gas condensing boiler\n",
    "\n",
    "<center>\n",
    "\n",
    "Specification | Index | Unit | Value\n",
    "--- | --- | --- | ---\n",
    "Thermal efficiency | $\\eta_{th}$ | [\\%] | 92\n",
    "Installation costs | $c_{GCB}^{tech}$ | [€] | 1500\n",
    "Variable costs | $c_{GCB}^{var,asset}$ | [€/$kW_{th}$] | 115\n",
    "Fuel costs | $c_{ng}^{fuel}$ | [ct. €/$kWh$] | 7\n",
    "Maintenance and insurance costs | $c_{mni}^{gcb}$ | [€/a] | 1000\n",
    "Expected asset lifetime | $T$ | [a] | 30\n",
    "\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Specifications for the pellet heater\n",
    "\n",
    "<center>\n",
    "\n",
    "Specification | Index | Unit | Value\n",
    "--- | --- | --- | ---\n",
    "Thermal efficiency | $\\eta_{th}$ | [\\%] | 90\n",
    "Installation costs | $c_{PEH}^{tech}$ | [€] | 2000\n",
    "Fixed capital costs | $c_{PEH}^{fix,asset}$ | [€] | 6000\n",
    "Variable costs | $c_{PEH}^{var,asset}$ | [€/$kW_{th}$] | 150\n",
    "Fuel costs | $c_{pellet}^{fuel}$ | [ct. €/$kWh$] | 7\n",
    "Maintenance and insurance costs | $c_{mni}^{gcb}$ | [€/a] | 1000\n",
    "Expected asset lifetime | $T$ | [a] | 30\n",
    "\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Specifications for the air-water heat pump\n",
    "\n",
    "<center>\n",
    "\n",
    "Specification | Index | Unit | Value\n",
    "--- | --- | --- | ---\n",
    "Coefficient of performance | $COP_{AHP}$ | [$kW_{th}$/$kW_{el}$] | 2.7\n",
    "Installation costs | $c_{AHP}^{tech}$ | [€] | 2000\n",
    "Fixed capital costs | $c_{AHP}^{fix}$ | [€] | 4000\n",
    "Variable costs | $c_{AHP}^{var,asset}$ | [€/$kW_{el}$] | 629\n",
    "Fuel costs | $c_{el}^{fuel}$ | [ct. €/$kWh$] | 30\n",
    "Maintenance and insurance costs | $c_{mni}^{AHP}$ | [€/a] | 200\n",
    "Expected asset lifetime | $T$ | [a] | 25\n",
    "\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Specifications for the brine-water heat pump\n",
    "\n",
    "<center>\n",
    "\n",
    "Specification | Index | Unit | Value\n",
    "--- | --- | --- | ---\n",
    "Coefficient of performance | $COP_{BHP}$ | [$kW_{th}$/$kW_{el}$] | 3.5\n",
    "Installation costs | $c_{BHP}^{tech}$ | [€] | 4000\n",
    "Fixed capital costs | $c_{BHP}^{probe}$ | [€] | 8000\n",
    "Variable costs | $c_{BHP}^{var,asset}$ | [€/$kW_{th}$] | 900\n",
    "Fuel costs | $c_{el}^{fuel}$ | [ct. €/$kWh$] | 30\n",
    "Maintenance and insurance costs | $c_{mni}^{gcb}$ | [€/a] | 200\n",
    "Expected asset lifetime | $T$ | [a] | 30\n",
    "\n",
    "</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tech_heat = {\n",
    "    \"gcb\" : {\n",
    "        \"commodity\" : \"natural_gas\",\n",
    "        \"eff_th\" : 0.92,\n",
    "        \"c_inst\" : 1500,\n",
    "        \"c_var\" : 115,\n",
    "        \"c_fuel\" : 7,\n",
    "        \"c_mni\" : 1000,\n",
    "        \"T\" : 30\n",
    "    },\n",
    "    \"pellet\" : {\n",
    "        \"commodity\" : \"pellet\",\n",
    "        \"eff_th\" : 0.90,\n",
    "        \"c_inst\" : 2000,\n",
    "        \"c_fix\" : 6000,\n",
    "        \"c_var\" : 150,\n",
    "        \"c_fuel\" : 7,\n",
    "        \"c_mni\" : 1000,\n",
    "        \"T\" : 30\n",
    "    },\n",
    "    \"aw_hp\" : {\n",
    "        \"commodity\" : \"electricity\",\n",
    "        \"COP\" : 2.7,\n",
    "        \"c_inst\" : 2000,\n",
    "        \"c_fix\" : 4000,\n",
    "        \"c_var\" : 629,\n",
    "        \"c_fuel\" : 26,\n",
    "        \"c_mni\" : 200,\n",
    "        \"T\" : 25\n",
    "    },\n",
    "    \"bw_hp\" : {\n",
    "        \"commodity\" : \"electricity\",\n",
    "        \"COP\" : 3.5,\n",
    "        \"c_inst\" : 4000,\n",
    "        \"c_fix\" : 8000,\n",
    "        \"c_var\" : 900,\n",
    "        \"c_fuel\" : 26,\n",
    "        \"c_mni\" : 200,\n",
    "        \"T\" : 25\n",
    "    }\n",
    "}"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 2.2 - Calculate CAPEX and OPEX\n",
    "Calculate the yearly costs of heat supply for a reinvest in the same heating technology. Assume a higher efficiency of the new gas condensing boiler with $\\eta_n$=97%, but same costs. The internal rate of return is $i$ = 5%.\n",
    "\n",
    "$$ CAPEX_{gcb} = c_{gcb}^{tech} + c_{gcb}^{var,asset} \\cdot P_{gcb} $$\n",
    "$$ OPEX_{gcb} = c_{gas}^{fuel} \\cdot \\frac{Q_{try}^{RH}+Q_{WW}}{\\eta_{th}} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### *gas condensing boiler*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class gcb:\n",
    "    def __init__(self, tech_dict: dict):\n",
    "        self.eff_th = tech_dict[\"eff_th\"]\n",
    "        self.c_inst = tech_dict[\"c_inst\"]\n",
    "        self.c_var = tech_dict[\"c_var\"]\n",
    "        self.c_fuel = tech_dict[\"c_fuel\"]\n",
    "        self.c_mni = tech_dict[\"c_mni\"]\n",
    "        self.lifetime = tech_dict[\"T\"]\n",
    "        \n",
    "    def CAPEX(self, P_inst):\n",
    "        return self.c_inst + self.c_var * P_inst\n",
    "    \n",
    "    # alternative\n",
    "    @staticmethod\n",
    "    def static_CAPEX(tech_dict: dict, P_inst):\n",
    "        return tech_dict[\"c_inst\"] + tech_dict[\"c_var\"] * P_inst\n",
    "\n",
    "    def OPEX(self, q_heat):\n",
    "        return self.c_fuel * q_heat/self.eff_th + self.c_mni\n",
    "    \n",
    "    # alternative\n",
    "    @staticmethod\n",
    "    def static_OPEX(tech_dict: dict, q_heat):\n",
    "        return tech_dict[\"c_fuel\"] * q_heat/tech_dict[\"eff_th\"] + tech_dict[\"c_mni\"]\n",
    "    \n",
    "    def TOTEX(self, i, P_inst, q_heat):\n",
    "        return annuity_factor(i, self.lifetime) * self.CAPEX(P_inst) + self.OPEX(q_heat)\n",
    "    \n",
    "    # alternative\n",
    "    @staticmethod\n",
    "    def static_TOTEX(tech_dict: dict, i, P_inst, q_heat):\n",
    "        return annuity_factor(i, tech_dict[\"T\"]) * tech_dict[\"c_inst\"] + tech_dict[\"c_var\"] * P_inst + tech_dict[\"c_fuel\"] * q_heat/tech_dict[\"eff_th\"] + tech_dict[\"c_mni\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the given example, the ``@staticmethod`` is like a special function that are grouped together with a class. They can be called without creating an object of the class. Using static methods can help with organizing code and keeping things tidy. They are useful when you have functions that don't need to know about specific objects or change any object's information. By using static methods, you make it clear that these functions are related to the class but can work independently without needing any specific object. It also makes testing these methods easier because they don't rely on object-specific information. Overall, static methods are a way to keep related functions together, make code organization simpler, and provide functions that can be used without creating objects."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Don't forget to update the improved efficiency for the gas condensing boiler:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tech_heat[\"gcb\"][\"eff_th\"] = 0.97"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For dimensioning the heating system we need to consider the maximum of the demand profile as the maximum power demand"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "max_heat_demand = round(max_heat_demand,0)\n",
    "max_heat_demand"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcb_potter = gcb(tech_heat[\"gcb\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can use our function to calculate CAPEX, OPEX and TOTEX:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gcb_capex = gcb_potter.CAPEX(P_inst=max_heat_demand)\n",
    "gcb_opex = gcb_potter.OPEX(q_heat=q_adj_rm_total)\n",
    "gcb_totex = gcb_potter.TOTEX(i=0.05, P_inst=max_heat_demand, q_heat=q_adj_rm_total)\n",
    "gcb_totex"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 2.3 - Consider different technologies\n",
    "**Task description:** Repeat the same for the different heating technologies."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### *pellet heater*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### *air-water heat pump*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to correctly dimension the heat-pump we need to consider the thermal peak load as well as the COP of the heat pump:\n",
    "\n",
    "$$ P_{inst} = \\frac{\\hat{Q}_{\\text{max}}}{\\text{COP}} $$\n",
    "\n",
    "**Task description:** Calculate the maximum installed power needed. Use the ``math.ceil()`` function to receive a uprounded integer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "math.ceil(4.567)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us first create an object for the air-water heat pump."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then execute operations on this object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### *brine-water heat pump*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check the installed capacity, as this heat pump is more efficient (which might reduced the installed capacity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 2.4 - Calculate Levelized cost of Heating\n",
    "Additionally calculate the LCOH for the four cases.\n",
    "\n",
    "$$ LCOH_{tech} = \\frac{TOTEX(ANN)_{tech}}{Q_{total}} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we define a function for that:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 2.5 - Identify cost efficient option\n",
    "\n",
    "**Task description:** Which invest option would you suggest for Family Potter, assuming they choose the cost efficient option.\n",
    "\n",
    "$$ \\text{lcoh}_{min} = \\min\\{\\text{LCOH}_{gcb}, \\text{LCOH}_{pellet}, \\text{LCOH}_{aw hp}, \\text{LCOH}_{bw hp}\\} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 2.6 - Investigate uncertainty\n",
    "Behind the decision to invest in a new heating technology is an inherent uncertainty due to the uncertain long-time price development of energy sources. Therefore, the Potter family questions whether their decision will be affected by changes in the price of commodities. Exemplarily, the gas prices rises to 15 ct. €/$kWh$.\n",
    "\n",
    "**Task description:** Recalculate LCOH by initiating a new object from the class ``gcb``. Then set the value of the variable ``c_fuel`` for the created object to the new value 15 and do the calculation for CAPEX, OPEX and TOTEX, follow by the LCOH."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task description:** Calculate the price increase in percentages."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
