import shutil
import sys
import os.path
import subprocess

HIDE = True

def install(package, manager, channel:str = "-c conda-forge") -> None:
    if manager == "pip":
        subprocess.check_call([sys.executable, "-m", manager, "install", package], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    elif manager == "conda":
        subprocess.check_call([sys.executable, "-m", manager, "install", channel, package], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

def check_installation(package) -> None:
    # Check if ipopt is installed
    try:
        subprocess.check_output(["conda", "list", package], stderr=subprocess.STDOUT)
        return True
    except subprocess.CalledProcessError:
        return False

# # Define the command as a list of arguments
# command = ['conda', 'update', '-n', 'base', 'conda']

# # Execute the command and hide the output
# subprocess.call(command, stdout=subprocess.DEVNULL)
        
# check if pyomo has been installed. If not, install with pip
pip_packages = ["demandlib", "workalendar"]
for package in pip_packages:
    if not shutil.which(package):
        install(package, "pip")
    # Call the check_ipopt_installation() function to check if ipopt is installed
    if check_installation(package):
        print(f"{package} is installed")
    else:
        print(f"{package} is not installed or installation is incomplete")

# check if ipopt is installed. If not, install.
conda_packages = ["gurobi"]
for package in conda_packages:
    if not (shutil.which(package) or os.path.isfile(package)):
        try:
            if conda_packages == "gurobi":
                install(package, "conda", "-c gurobi")
            else:
                install(package, "conda")
        except:
            pass
    # Call the check_ipopt_installation() function to check if ipopt is installed
    if check_installation(package):
        print(f"{package} is installed")
    else:
        print(f"{package} is not installed or installation is incomplete")