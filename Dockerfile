# Specify parent image. Please select a fixed tag here.
#ARG BASE_IMAGE=registry.git.rwth-aachen.de/jupyter/profiles/rwth-courses:latest
ARG BASE_IMAGE=registry.jupyter.rwth-aachen.de/profiles/rwth-courses:latest
FROM ${BASE_IMAGE}

ARG conda_env=python310
ARG py_ver=3.10.8
ARG disp_name="Python 3.10 (ipykernel)"

RUN mamba create --yes -p "${CONDA_DIR}/envs/${conda_env}" python=${py_ver} ipython ipykernel && mamba clean --all -f -y

RUN "${CONDA_DIR}/envs/${conda_env}/bin/python" -m ipykernel install --prefix=${CONDA_DIR} --name="${conda_env}" --display-name "${disp_name}" && fix-permissions "${CONDA_DIR}" && fix-permissions "/home/${NB_USER}"

ADD requirements.txt .
RUN "${CONDA_DIR}/envs/${conda_env}/bin/pip" install --no-cache-dir -r requirements.txt


# .. Or update conda base environment to match specifications in environment.yml
ADD environment.yml /tmp/environment.yml

# All packages specified in environment.yml are installed in the base environment
RUN mamba env update -f /tmp/environment.yml && \
    mamba clean -a -f -y

# Install packages via requirements.txt
#ADD requirements.txt .
#RUN pip install -r requirements.txt
